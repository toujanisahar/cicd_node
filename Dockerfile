FROM node:current-alpine3.17

COPY . .

RUN npm install 

CMD ["node", "index.js"]